# The Environment

The bash shell defines a computing environment which can be customized using environment variables.

The command echo can be used to expand the environmental variable using the *$* in front of the variable name:

    echo $USERNAME
    echo $PWD
    echo $PATH
    

USERNAME, PWD, PATH are  enviormental variables  

In bash you can create your variables and also change variables with the export command::

    export DATAPATH=~/PATH
    
Variables are case-sensitive, so try:

    # should not get anything
    echo $datapath
    
    # This should work
    echo DATAPATH
    

Some common variable names include:
    
    USER -  User name
    PATH - List of absolute paths that are searched for executables
    PWD - Current directory (short for print working directory)
    EDITOR - Default text editor
    GROUP - Groups the user belongs to
    HOME - Home directory
    ~ - Same as HOME
    DISPLAY -  Used in forwarding graphics over a network connection
    LD_LIBRARY_PATH - Like PATH, but for precompiled libraries
    FC - Fortran compiler
    CC - C compiler

To see all environment variables that are active use the *env* command::

    env
    
To make environment variable activity every time you open a new terminal, you must add it to a file in the home director .bashrc or .bash_profile

.bash_profile is executed for login shells while .bashrc is executed for interactive non-login shells.  Typically both are run each interactive shell but can be configured.



## Saving Environment Variables (.bashrc)

Note that the *.* in .bashrc makes it a hidden file so to search for hidden files type::

    ls -a
    
The .bash_profile or .profile file should point to the .bashrc file


Do the following:
* Use your text editor to open the .bashrc file in your home directory. If no such file exists, create it.
* Add an export command to set a variable called DATA equal to the location of some data on your filesystem.
* Open a new terminal window and query the DATA variable with echo.
* What is the result of cd $DATA? Can you imagine ways this behavior could make navigating your files easier?


    alias f="ssh -Y falcon2"
    alias l='ssh -Y lemhi2'
    DATAPATH="/Users/petejl/MY_MCNP/ATR_MCNP_DATA/mcnpxs/"

When you change the .bashrc you can rerun it without having to open a new terminal using the command:

    source .bashrc

The bash shell can be customized for many things with one of the most import things being the PATH

## Running Programs (PATH)

If you want to run a program from a nonstandard location, you will have to tell the shell exactly where the program is located with either an absolute or relative path

Even if you are in the directory where the program is located you will have to specify the relative path with a leading.

    ./[program_name]
    
For the computer to find the program without typing a full path, the PATH environment variable must contain the directory of the program

You can add files to the path using the command line:

    export PATH=$PATH:/[program_folder]

EXERCISE: CUSTOMIZE YOUR PATH:
* In the terminal, use echo to determine the current value of the PATH environment variable. Why do you think these directories are in the PATH?
* Use export to add your current directory to the end of the list. Don’t forget to include the previous value of PATH.
* Use echo once again to determine the new value.

## Nicknaming Commands (alias)

Same way as creating a variable for shortening long strings ($DATA the path to your data) you can generate shorthand aliases for commands.  It simple replaces the first argument by the  second

For example:

    alias ls 'ls --color'

One the command has been executed in the terminal, typing ls is equivalent to typing ls --color.  If you want it activity every time you open a new terminal you need to add it to the .bashrc file

Some people keep the aliases in a different folder called .bash_aliases to keep the .bashrc clean.  This is adding the following to the .bashrc

    source ~/.bash_aliases 
    
And then creating .bash_aliases using :

    ####################### BASH ALIASES #######################
     # GENERAL COMMANDS
     alias l='ls -l'
     alias ll='ls -la'
     alias l.='ls -d .*'
    
     # Shortcuts
     alias cl='clear'
     alias j='jobs -l'
     alias ..='cd ..'

     # SYSINFO
     alias meminfo='free -m -l -t'
     alias cpuinfo='lscpu'
    
     # TOP MEM EATING PROCESSES
     alias psmem='ps auxf | sort -nr -k 4'
     alias psmem10='ps auxf | sort -nr -k 4 | head -10'
    
     # TOP CPU EATING PROCESSES
     alias pscpu='ps auxf | sort -nr -k 3'
     alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
     alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'
    
   
     # Update
     alias apt_upd='sudo apt-get update && sudo apt-get upgrade'
     alias apt_clean='sudo apt-get autoclean && apt-get autoremove'
    
    

##