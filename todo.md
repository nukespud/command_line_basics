TODO:
=====

- [Intial setup](initial_setup.md)

- [Navigating the shell](navigating_the_shell.md)

- [Manipulating Files and Directores](creating_files.md)

- [Getting help](getting_help.md)

- [Permissions and Sharing](permissions_and_sharing.md)

- [The Environment](the_environment.md)

- [Scripting with Bash](scripting_with_bash.md)
    * Come up with a better scripting example with MCNP or such
    
    