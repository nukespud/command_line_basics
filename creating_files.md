
# Creating Files (nano, emacs, vi, cat, >, and touch)

Creating files can be done in a few ways:


* With a graphical user interface (GUI) outside the terminal (like Notepad, Eclipse, UltraEdit, or the Jupyter Notebook)
* With the *touch* command
* From the command line with cat and redirection (>)
* With a sophisticated text editor inside the terminal, like nano, emacs, or vi

## GUIs for File Creation

Source code files are plain-text files and should not be created in Microsoft Word and other similar programs.
Typically we use an interactive development environment (IDEs) based on the type of source .
* IDEs can assist with the syntax of the individual language and produce plain text
* IDEs can have autocompleted and excellent searching capability along with built-in version control systems like git
* IDEs typically require the user to leave the terminal.

Some recomended IDEs include:


* [Pycharm - python](https://www.jetbrains.com/pycharm/)
* [Sypder - python](https://www.spyder-ide.org)
* [Sublimetext](https://www.sublimetext.com)
* [Netbeans](https://netbeans.org)
* [Visual Studio Code](https://code.visualstudio.com)


## Creating an Empty File (Touch)

An empty text file can be created with the touch command.

    touch empty_file.txt
    ls
    
If the file already exists it will just update the metadata such as the timestamp

<aside class="warning">
Note: empty_file had an underscore because spaces are problematic for shell and should be avoided.  But is needed space can be created using the ("\ ") command such as
</aside>

        touch empty\ file.txt
        ls



## The most straightforward text editor (Cat and >)

The most straightforward text editor is to use *cat* (concatenate) to create a file.  

    echo "# Fission Power Idea

    The heat from the fission reaction could be used to heat fluids. In the same way that coal power starts with the production heat which turns water to steam and spins a turbine, so too nuclear fission might heat a fluid that pushes a turbine. If somehow there were a way to
    have many fissions in one small space, the heat from those fissions could be used to heat quite a lot of water.
    
    Of course, it would take quite a lot of fissions.
    
    Perhaps Professors Rutherford, Curie, or Fermi have some ideas on this topic." > fission.txt


Then you can create a new file with *cat* or append to an existing file.

    cat fission.txt > fission_copy.txt
    echo "****New file******" >> fission_copy.txt
    cat fission.txt >> fission_copy.txt


<aside class="warning">
You can terminate a never-terminationg program using Cntrl-c to excape 
</aside>

try:

    yes
    Ctrl-c

## More Powerful Text Editors (Nano, EMACS, and VIM)

Shell text editors are an easy way to make quick changes to a document.  Some people can even get more work done in text editors such as vim then a GUI editor such as notebook because of all the short cuts.

However, because they are so powerful, many text editors have a steep learning curve.

Of the three text editors *nano* is the easiest to use.  To try type:

    nano fission_copy.txt  # Not available on copy.sh web emulator
    
    # If you don't have nano installed try vi
    vi fission_copy.txt  # Type :q to exit
    
[VI Cheat sheet](http://web.mit.edu/merolish/Public/vi-ref.pdf)

[Another VI Cheat sheet](http://www.atmos.albany.edu/daes/atmclasses/atm350/vi_cheat_sheet.pdf)
 
## Practice problem:

1. open fission.txt with nano or vi
2. add the text "E=mc^2 is the most famous equation in the world." 
3. use cat and > to add fission.txt input to fission_copy.txt
4.  open fission_copy.txt to make sure it was added correctly 