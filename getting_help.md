# Getting Help

Now that you have become familiar with the basics, you can freely explore the terminal. The most important thing to know before venturing forth, however, is how to get help.


## Reading the Manual (man)

The program man (manual) is an interface to online reference manuals. If you pass the name of a command or program to man as an argument, it will open the help file for that command or program. To determine what flags and options are available to the ls command, then, typing man ls would provide the instructions for its use. Since man is itself a program, we can type man man to get the directions for using man:

    man man  # not avaliable on copy.sh emulator
    
    
You will get the following:

    NAME
           man - an interface to the on-line reference manuals
    
    SYNOPSIS
           man  [-c|-w|-tZ]  [-H[browser]]  [-T[device]] [-adhu7V]
           [-i|-I] [-m system[,...]] [-L locale] [-p  string]  [-C
           file]  [-M  path]  [-P pager] [-r prompt] [-S list] [-e
           extension] [[section] page ...] ...
           man  -l  [-7]  [-tZ]  [-H[browser]]  [-T[device]]   [-p
           string] [-P pager] [-r prompt] file ...
           man -k [apropos options] regexp ...
           man -f [whatis options] page ...
    
    DESCRIPTION
           man  is  the  systems manual pager. Each page argument
           given to man is normally the name of a program, utility
           or  function.   The manual page associated with each of
           these arguments is then found and displayed. A section,
           if  provided, will direct man to look only in that sec
           tion of the manual.  The default action is to search in
           all  of the available sections, following a pre-defined
           order and to show only the first page  found,  even  if
           page exists in several sections.
    
    
    <snip>
    
## Arguments, Options, and Variables

* *argument:* gets added after the command (can have multiple arguments if the command expects it)

        # One argument
        touch temp.txt
        
        # Two arguments
        cp temp.txt temp2.txt
    

* *options:* (flags or switches) tell the program to run in some predefined way (typically *-*).  

        ls .     # Normal list command
        ls -r .  # Run in reverse order
    

* *Variables:* can be used to pass specific kinds of information 

        find . -name fission.txt

<aside class="warning">

NOTE: Flags do not always have the same meaning for every command for example -r is reverse for the directory but recursive for others

</aside>

## Moving around in less

man command opens up the command in the program called *less* which has some features:

* Use the up and down arrows to scroll up and down.
* Use Page Up and Page Down (or the space bar) to move up or down by an entire page.
* Use a forward slash (*/*) followed by a search term and then Enter to search for a particular word. The letter n (next) toggles through each occurrence.
* Use *h* to display help inside less—this displays all the possible commands that less understand.
* Use *G* to go to the last line
* Use *q* to quit.


### Exercise: Use the Man page with less

Run one of these commands are explore it with the less

    man mkdir
    man touch
    man scp
    man mv
    
    # For website version
    less fission_copy.txt

## Finding the Right Hammer (apropos)

You can search the man pages for keywords with a command called apropos. Let’s say you want to know what text editors are available. You might search for the string “text editor”:

    apropos "text editor"  # Not on the website version
    apropos "game"

## Combining Utilities with Redirection and Pipes (>, >>, and |)


The power of the shell lies in the ability to combine these simple utilities into more complex algorithms very quickly. A key element of this is the ability to send the output from one command into a file or to pass it directly to another program.

* One arrow (>) will create a new file or overwrite an exsting file. 
* Two arrows (>>) will append the stream to end of an existing file:

        echo $PATH > path.txt
        vi path.txt
        echo "# Overwriting path" > path.txt
        vi path.txt   
        echo $PATH >> path.txt
        vi path.txt
        

The pipe (|) changes programs together instead of saving to an existing file:

        head -l path.txt 
        head -l path.txt | tail -l
        
        
        
    
 
