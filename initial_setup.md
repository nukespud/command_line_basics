# Getting shell setup on your computer


* If you have a Mac or a Linux computer, then it comes with a shell.  You can get access it by running Terminal or an equivalent program such as iTerm2.

* For Windows, you may need to do one of the following:

	* download the shell at [cmder](https://cmder.net) (download Mini version for this tutorial)

	* use an online such as [Copy.sh](https://copy.sh/v86/?profile=linux26)

    * more info on other online emulators (https://itsfoss.com/online-linux-terminals/)

