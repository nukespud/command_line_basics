# Navigating the Shell


One very basic command is `echo`. This command is just prints text to
the terminal. Try entering the command:

    echo Hello, World

Then press enter. You should see the text "Hello, World" printed back
to you. The echo command is useful for 

## Moving around the file system

Let's learn how to move around the file system using command line
programs. This is really easy to do using a GUI (just click on
things). Once you learn the basic commands, you'll see that it is
really easy to do in the shell too. 

First we have to know where we are. The program `pwd` (print working
directory) tells you where you are sitting in the directory tree. The
command `ls` will list the files in files in the current
directory. Directories are often called "folders" because of how they
are represented in GUIs. Directories are just listings of files. They
can contain other files or directories.

Whenever you start up a terminal, you will start in a special
directory called the *home* directory. Every user has their own home
directory where they have full access to do whatever they want. In
this case, the `pwd` command tells us that we are in the `/home/thw`
directory. This is the home directory for the `thw` user. That is our
user name. You can always find out your user name by entering the
command `whoami`. 

### File Types

When you enter the `ls` command lists the contents of the current
directory. There are several items in the home directory, notice that
they are all colored blue. This tells us that all of these items are
directories as opposed to files.

Lets create an empty file using the `touch` command. Enter the
command:

    touch testfile

Then list the contents of the directory again. You should see that a
new entry, called `testfile`, exists. It is colored white meaning that
it is a file, as opposed to a directory. The `touch` command just
creates an empty file. 

Some terminals will not color the directory entries in this very
convenient way. In those terminals, use `ls -F` instead of `ls`. The
`-F` argument modifies the results so that a slash is placed at the
end of directories. If the file is *executable* meaning that it can be
run like a program, then a star fill be placed of the file name.

You can also use the command `ls -l` to see whether items in a
directory are files or directories. `ls -l` gives a lot more
information too, such as the size of the file and information about
the owner. If the entry is a directory, then the first letter will be
a "d". The fifth column shows you the size of the entries in
bytes. Notice that `testfile` has a size of zero.

Now, let's get rid of `testfile`. To remove a file, just enter the
command:

    rm testfile

The `rm` command can be used to remove files. If you enter `ls` again,
you will see that `testfile` is gone.


### Changing Directories

Now, let's create a directory called first_folder. The command `cd` (change
directory) is used to move around. Let's move into the `first_folder`
directory. Enter the following command:

    mkdir first_folder


    cd first_folder

Now use the `ls` command to see what is inside this directory. You
will see that there is an entry which is green. This means that this
is an executable. If you use `ls -F` you will see that this file ends
with a star.


If you enter the `cd` command by itself, you will return to the home
directory. Try this, and then navigate back to the `1-Shell`
directory.

## Arguments

Most programs take additional arguments that control their exact
behavior. For example, `-F` and `-l` are arguments to `ls`.  The `ls`
program, like many programs, take a lot of arguments. But how do we
know what the options are to particular commands?

Most commonly used shell programs have a manual. You can access the
manual using the `man` program. Try entering:

    man ls

This will open the manual page for `ls`. Use the space key to go
forward and b to go backwards. When you are done reading, just hit `q`
to exit.

Programs that are run from the shell can get extremely complicated. To
see an example, open up the manual page for the `mplayer` program,
which is command line driven video player. There are about 300
arguments to the mplayer command. No one can possibly learn all of
these arguments, of course. So you will probably find yourself
referring back to the manual page frequently.

### Examining the contents of other directories

By default, the `ls` commands lists the contents of the working
directory (i.e. the directory you are in). You can always find the
directory you are in using the `pwd` command. However, you can also
give `ls` the names of other directories to view. Navigate to the
home directory if you are not already there. Then enter the
command:

    ls /usr

This will list the contents of the `bin` directory without
you having to navigate there. Now enter:

    ls usr/bin

This prints the contents of `bin`. The `cd` command works in a
similar way. Try entering:

    cd usr/bin

and you will jump directly to `bin` without having to go through
the intermediate directory.

## Full vs. Relative Paths

The `cd` command takes an argument which is the directory
name. Directories can be specified using either a *relative* path a
full *path*. The directories on the computer are arranged into a
hierarchy. The full path tells you where a directory is in that
hierarchy. Navigate to the home directory. Now, enter the `pwd`
command and you should see:

    /usr/bin

which is the full name of your home directory. This tells you that you
are in a directory called `bin`, which sits inside a directory called
`usr` which sits inside the very top directory in the hierarchy. The
very top of the hierarchy is a directory called `/` which is usually
referred to as the *root directory*. So, to summarize: `bin` is a
directory in `usr` which is a directory in `/`.

Now enter the following command:

    mkdir first_folder
    cd ~/first_folder  

This jumps to `first_folder`. Now go back to the home directory. We saw
earlier that the command:

    cd 
    ls
    cd first_folder

had the same effect - it took us to the `1-Shell` directory. But,
instead of specifying the full path
(`~/first_folder/`), we specified a *relative path*. In
other words, we specified the path relative to our current
directory. A full path always starts with a `/`. A relative path does
not. You can usually use either a full path or a relative path
depending on what is most convenient. If we are in the home directory,
it is more convenient to just enter the relative path since it
involves less typing.

Now, list the contents of the /bin directory. Do you see anything
familiar in there?


## Saving time with shortcuts, wild cards, and tab completion

### Shortcuts

There are some shortcuts which you should know about. Dealing with the
home directory is very common. So, in the shell the tilde character,
`~`, is a shortcut for your home directory. Navigate to the `1-Shell`
directory, then enter the command:

    ls ~

This prints the contents of your home directory, without you having to
type the full path. The shortcut `..` always refers to the directory
above your current directory. Thus: 

    ls ..

prints the contents of the `~/first_folder`. You can chain
these together, so:

    ls ../../

prints the contents of `/home/thw` which is your home
directory. Finally, the special directory `.` always refers to your
current directory. So, `ls`, `ls .`, and `ls ././././.` all do the
same thing, they print the contents of the current directory. This may
seem like a useless shortcut right now, but we'll see when it is
needed in a little while.

To summarize, the commands `ls ~`, `ls ~/.` all do exactly the same thing. These shortcuts are not
necessary, they are provided for your convenience.

### Wild cards

Navigate to the `/usr/bin/` directory. If we type `ls`,
we will see that there are a bunch of files . By default, `ls` lists all of the files in a given
directory. The `*` character is a shortcut for "everything". Thus, if
you enter `ls *`, you will see all of the contents of a given
directory. Now try this command:

    ls l*

This lists every file that starts with a `1`. This command:

    ls /usr/bin/dir*

Lists every file in `/usr/bin` that starts in the characters `dir`. And
this command:

    ls /usr/bin/*ess*


So how does this actually work? Well...when the shell (bash) sees a
word that contains the `*` character, it automatically looks for files
that match the given pattern. In this case, it identified four such
files. Then, it replaced the `*4*1` with the list of files, separated
by spaces. In other the two commands:

    ls /usr/bin/*ess*

are exactly identical. The `ls` command cannot tell the difference
between these two things.

* * * *
### Short Exercise

Do each of the following using a single `ls` command without
navigating to a different directory.

1.  List all of the files in `/usr/bin` that contain the letter `a`
2.  List all of the files in `/usr/bin` that contain the letter `a` or the letter `b`
3.  List all of the files in `/usr/bin` that contain the letter `a` AND the letter `b`

* * * *

### Tab Completion

Navigate to the home directory. Typing out directory names can waste a
lot of time. When you start typing out the name of a directory, then
hit the tab key, the shell will try to fill in the rest of the
directory name. For example, enter:

    ls U<tab>

The shell will fill in the rest of the directory name for
`first_folder`. Now enter:

    ls D<tab><tab>

When you hit the first tab, nothing happens. The reason is that there
are multiple directories in the home directory which start with
D. Thus, the shell does not know which one to fill in. When you hit
tab again, the shell will list the possible choices. 

Tab completion can also fill in the names of programs. For example,
enter `e<tab><tab>`. You will see the name of every program that
starts with an `e`. One of those is `echo`. If you enter `ec<tab>` you
will see that tab completion works.

## Which program? ##

Commands like `ls`, `rm`, `echo`, and `cd` are just ordinary programs
on the computer. A program is just a file that you can *execute*. The
program `which` tells you the location of a particular program. For
example:

    which ls

Will return "/bin/ls". Thus, we can see that `ls` is a program that
sits inside of the `/bin` directory. Now enter:

    which which

You will see that `which` is a program that sits inside of the
`/usr/bin` directory.

So ... when we enter a program name, like `ls`, and hit enter, how
does the shell know where to look for that program? How does it know
to run `/bin/ls` when we enter `ls`. 
 


## Creating, moving, copying, and removing

Created a file called `fission.txt`.  Assume this is critical file so we have to make copies so that the data is backed up. Lets copy the file using the `cp` command. The `cp` command backs up the file. Navigate to the `data` directory and enter:

    cp fission.txt fission_backup

Now `fission_backup` has been created as a copy of `fission.txt`. We can
move files around using the command `mv`. Enter this command:

    mv fission_backup /tmp/

This moves `fission_backup` into the directory `/tmp`. The directory
`/tmp` is a special directory that all users can write to. It is a
temporary place for storing files. Data stored in `/tmp` is
automatically deleted when the computer shuts down.

The `mv` command is also how you rename files. Since this file is so
important, let's rename it:

    mv fission.txt fission_IMPORTANT

Now the file name has been changed to fission_IMPORTANT. Let's delete
the backup file now:

    rm /tmp/fission_backup

The `mkdir` command is used to create a directory. Just enter `mkdir`
followed by a space, then the directory name. 

* * * *
### Short Exercise


Do the following:

1.  Create a directory called `info`
2.  Then, copy the `fission.txt` file into `info`

* * * *

By default, `rm`, will NOT delete directories. You can tell `rm` to
delete a directory using the `-r` option. Enter the following command:

    rm -r info





## Searching files

You can search the contents of a file using the command `grep`. The
`grep` program is very powerful and useful especially when combined
with other commands by using the pipe. Navigate to the `Bert`
directory. Every data file in this directory has a line which says
"Range". The range represents the smallest frequency range that can be
discriminated. Lets list all of the ranges from the tests that Bert
conducted:

    grep fission *



## Finding files

The `find` program can be used to find files based on arbitrary
criteria. Navigate to the `data` directory and enter the following
command:

    find .

This prints the name of every file or directory, recursively, starting
from the current directory. Let's exclude all of the directories:

    find . -type f

This tells `find` to locate only files. Now try these commands:

    find . -type f -name "*s*"
    find . -type f -name "*s*" -or -name "*2*"
    find . -type f -name "*s*" -and -name "*2*"

The `find` command can acquire a list of files and perform some
operation on each file. Try this command out:

    find . -type f -exec grep fission {} \;

This command finds every file starting from `.`. Then it searches each
file for a line which contains the word "Volume". The `{}` refers to
the name of each file. The trailing `\;` is used to terminate the
command.


