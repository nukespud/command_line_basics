# Scripting with Bash

Repeated commands can be stored and executed at will.  Any series of commands can be placed into a file called a *script*.  This file (like any program) can be written just once and executed many times

Bash scripts typically end in .sh so to create a script do the following:

    vi automate.sh
    
Any commands in a terminal are valid in a bash script.  Some might be for a reference and can be commented out with *#*

An example of a script is as follows:

    # automate.sh 
    # explore the three directories above this one
    # print a status message
    echo "Initial Directory:"
    # print the working directory
    pwd
    # list the contents of this directory
    ls
    echo "Parent Directory:"
    # ascend to the parent directory
    cd ..
    pwd
    ls
    echo "Grandparent Directory:"
    cd ..
    pwd
    ls
    echo "Great-Grandparent Directory:"
    cd ..
    pwd
    ls
    
After the file is saved we need to change the permission to make it executable:

    chmod a+x automate.sh
    
Now the *automate.sh* is runnable.  To run the command type in the following:

    ./explore.sh
    
        