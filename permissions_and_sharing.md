# Permissions and Sharing

Permissions are an essential part of using and sharing files and using commands on Unix, Linux, and Mac systems

The idea is that different people can be given different types of access to a given file, program, or computer.



## Seeing Permissions (ls -l)

The command *ls* is a way to list contents of a directory.  With the man page, there is a -l flag which is the long format (including permissions)

If we run:

    ls -l
    
You will get something like:

    -rwxr-xr-x  1 petejl  staff   1144 Jul  9 12:31 scripting_with_bash.md
    drwxr-xr-x  2 petejl  staff     64 Jul  9 15:16 temp
    lrwxr-xr-x  1 petejl  staff      4 Jul  9 15:16 temp1 -> temp



The first ten characters describe the permission for the directory content for both files and directories

First entry is (d : directory, l : link, - : other )
Next three indicate that the **user owner** (petejl) can read(r) or write(w) or execute (x) the file
The following three characters are for the permssion of the **group owner** (staff) read(r) or can not write(-) or execute (x)
The final three characters are from everyone else (r-x) read(r) or can not write(-) or execute (x)

To figure out which group you are in and your user name you can type:

    # User name
    whoami
    # Group names
    id -Gn
    # Detailed info
    id
    

## Setting ownership (chown)

You can change the ownership of the file using chown.  Type 

    man chown

You get the following info.

    NAME
         chown -- change file owner and group
    
    SYNOPSIS
         chown [-fhv] [-R [-H | -L | -P]] owner[:group] file ...
         chown [-fhv] [-R [-H | -L | -P]] :group file ...


Note: directories that are not executable by a given user can’t be navigated into, and directories that aren’t readable by a given user can’t be printed with ls.

## Setting Permissions (chmod)

Use chmod to change the user setting:

    man chmod
    
A combination of the letters ugoa controls which users' access to the file will be changed: the user who owns it (u), other users in the file's group (g), other users not in the file's group (o), or all users (a)

The operator + causes the selected file mode bits to be added to the existing file mode bits of each file; - causes them to be removed, and = causes them to be added and causes unmentioned bits to be removed except that a directory's unmentioned set user and group ID bits are not affected.


This command adds r and x to the fill and recursive for all other files

    chmod -R g+rx

Note:  Need to be careful when changing permissions because you could remove your access to the file 


## Creating Links (ln)

Symbolic links are useful for providing access to large, shared resources, rather than storing multiple copies in multiple hard-to-reach locations. 

The ln command allows a user to create a hard or symbolic link to a file or program.

The safest and most useful link is the soft link *ln -s*

ln can be useful when a file is looking for something locally such as a surface source and instead of copying it over and over the user can use a symbolic link then the computer will be able to use it locally, and no additional memory issues will have to be dealt with

    ln -s wssa_saved rssa
    
You can see where the symbolic link is pointing based on the command:

    ls -l
    
   

## Connecting to Other Computers (ssh and scp)

Since most large high-performance or high-throughput computing resources can only be accessed by SSH (Secure SHell) or similar protocols through the command line, truly high-powered computer systems are not accessible without the use of the shell.

The command you would use is::

    ssh -Y [user_name]@[computer_address]
    
Once logged into the computer then you can do all of the same command line items on a different network

You can also copy folders using the scp (secure copy) command.  The syntax is:

    scp <source_file> [[user@host]:<destination> 
    
    # or 
    
    scp [[user@host]:<source_file> <destination> 
    
    
Note:  Both ssh and scp required a valid username and password on a remote machine

When connected to another computer there are not only different files but also different environments