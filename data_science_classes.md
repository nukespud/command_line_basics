# Classes register on eventbrite (password data_science):
    

## July 18: 2 - 4 - Excell, Word, and Pdf with Python

https://www.eventbrite.com/e/data-science-working-meeting-at-caes-booster-workshop-2-working-with-excel-spreadsheets-pdfs-and-tickets-64205907598?ref=estw
 
(password data_science)
    
## July 29: 10-12 - Data analysis and visualization with python

 https://www.eventbrite.com/e/data-science-working-meeting-at-caes-booster-workshop-4-data-analysis-and-visualization-with-python-tickets-64206835373?ref=estw
 
 (password data_science)
 
## August 6-7 : Introduction to data science and machine learning

https://www.eventbrite.com/e/data-science-working-meeting-at-caes-2-days-tickets-65269183886

(password data_science)
## August 9 : Machine learning (for those that know python)

https://www.eventbrite.com/e/data-science-working-meeting-at-caes-2nd-day-tickets-65269542960

(password data_science)
## August 13: 2-4 Data storage with python using HDF5, SQL, and Pandas

(password data_science)

https://www.eventbrite.com/e/data-science-working-meeting-at-caes-booster-workshop-4-data-storage-with-python-using-hdf5-sql-and-tickets-65268775665
