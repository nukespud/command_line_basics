# Introduction to the command line

Most of this material came from book [Effective Computation in Physics](http://lilith.fisica.ufmg.br/~dickman/transfers/comp/textos/Effective%20Computation%20in%20Physics%20(Python).pdf) Chapter 1 by Katy Huff and Anthony Scopatz

Also from this material came from a presentation by [Milad Fatenejad] <https://github.com/thehackerwithin/UofCSCBC2012/tree/master/1-Shell/>  

---
[Introduction page](data_science_classes.md)

---

## Overview of the tutorial

- [Intial setup](initial_setup.md)
- [Creating Files](creating_files.md)
- [Navigating the shell](navigating_the_shell.md)
- [Getting help](getting_help.md)
- [Permissions and Sharing](permissions_and_sharing.md)
- [The Environment](the_environment.md)
- [Scripting with Bash](scripting_with_bash.md)
